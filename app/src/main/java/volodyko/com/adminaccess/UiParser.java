package volodyko.com.adminaccess;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.List;

/**
 * Created by volodyko on 18.03.16.
 */
public class UiParser {
    private static final String TAG = "UiParser";
    private int lastLocalizedStringId;
    private Context context;

    public UiParser(Context base) {
        context = base;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public AccessibilityNodeInfo findNodeByText(AccessibilityNodeInfo paramAccessibilityNodeInfo, String paramString) {
        AccessibilityNodeInfo resultNode;
        if (TextUtils.isEmpty(paramString) || paramAccessibilityNodeInfo == null) {
            resultNode = null;
        } else {
            Log.d(TAG, "Searching for button with text: " + paramString);
            List findedNodes = paramAccessibilityNodeInfo.findAccessibilityNodeInfosByText(paramString);
            if (findedNodes != null && !findedNodes.isEmpty()) {
                Log.d(TAG, "  Found " + findedNodes.size());
                while (findedNodes.size() > 1) {
                    ((AccessibilityNodeInfo) findedNodes.remove(findedNodes.size() - 1)).recycle();
                }
                return (AccessibilityNodeInfo) findedNodes.get(0);
            }
            resultNode = parseComplexNode(paramAccessibilityNodeInfo, new NodeInfo(paramString));
            if (resultNode == null) {
                Log.d(TAG, "  Not found");
            } else {
                Log.d(TAG, "findNodeByText: " + resultNode.toString());
            }
        }

        return resultNode;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public AccessibilityNodeInfo parseComplexNode(AccessibilityNodeInfo node, INodeInfo nodeInfo) {
        for (int i = 0; i < node.getChildCount(); ++i) {
            AccessibilityNodeInfo curentNode = node.getChild(i);
            if (curentNode != null) {
                if (nodeInfo.compare(curentNode)) {
                    return curentNode;
                }
                AccessibilityNodeInfo childNode = parseComplexNode(curentNode, nodeInfo);
                curentNode.recycle();
                if (childNode != null) {
                    return childNode;
                }
            }
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public AccessibilityNodeInfo findNodeById(AccessibilityNodeInfo localAccessibilityNodeInfo, String textToFind) {
        AccessibilityNodeInfo resultNode = null;
        if (Build.VERSION.SDK_INT >= 18) {
            Log.d(TAG, "Searching for " + textToFind);
            List findedNodes = localAccessibilityNodeInfo.findAccessibilityNodeInfosByViewId(textToFind);
            if (findedNodes != null && !findedNodes.isEmpty()) {
                resultNode = (AccessibilityNodeInfo) findedNodes.get(0);
                Log.d(TAG, "  Found " + findedNodes.size() + ", [0] " + resultNode.getText());
                while (findedNodes.size() > 1) {
                    ((AccessibilityNodeInfo) findedNodes.remove(findedNodes.size() - 1)).recycle();
                }
            } else {
                Log.d(TAG, "  Not found");
                resultNode = null;
            }
        }

        return resultNode;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean isNodeVisible(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
        return paramAccessibilityNodeInfo.isVisibleToUser();
    }

    public String getLocalizedText(ComponentName componentName, String paramString) {
        String resultString = "";
        Log.d(TAG, "Querying text of " + paramString);
        try {
            Resources localResources = context.getPackageManager().getResourcesForApplication(componentName.getPackageName());
            int stringId = localResources.getIdentifier(paramString, "string", componentName.getPackageName());
            if (stringId == 0) {
                Log.d(TAG, "  Not found");
                resultString = null;
            } else {
                if (lastLocalizedStringId == 0) {
                    lastLocalizedStringId = stringId;
                }
            }
            paramString = localResources.getString(stringId);
            resultString = paramString;

        } catch (Exception exception) {
            Log.e(TAG, exception.getMessage().toString());
        }
        return resultString;
    }

}
