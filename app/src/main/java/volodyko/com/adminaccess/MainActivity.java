package volodyko.com.adminaccess;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_ENABLE_ADMIN = 12;
    ComponentName deviceAdminComponentName;
    DevicePolicyManager devicePolicyManager;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(this, DeviceAdminSampleReceiver.class);

        findViewById(R.id.get_accessibility).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }
        });

        findViewById(R.id.app_usage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent msgRcv = new Intent(SampleAccessibilityService.INTENT_FILTER);
                msgRcv.putExtra(SampleAccessibilityService.COMMAND_INTENT, SampleAccessibilityService.USAGE_STATS);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(msgRcv);

                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivity(intent);
            }
        });

        findViewById(R.id.text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean isActive = devicePolicyManager.isAdminActive(deviceAdminComponentName);

                if (!isActive) {
                    Intent msgRcv = new Intent(SampleAccessibilityService.INTENT_FILTER);
                    msgRcv.putExtra(SampleAccessibilityService.COMMAND_INTENT, SampleAccessibilityService.ADMIN_ACCESS);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(msgRcv);

                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminComponentName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "You must enable device administration for certain features"
                            + " of the app to function.");
                    startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
                } else {
                    devicePolicyManager.removeActiveAdmin(deviceAdminComponentName);
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ENABLE_ADMIN:
                if (resultCode == RESULT_OK) {
                    Log.d(TAG, "onActivityResult: OK");
                } else {
                    Log.d(TAG, "onActivityResult: FAIL");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
