package volodyko.com.adminaccess;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.accessibility.AccessibilityNodeInfo;

/**
 * Created by volodyko on 17.03.16.
 */
public class NodeInfo implements INodeInfo {
    private final String nodeText;

    public NodeInfo(String nodeText) {
        this.nodeText = nodeText;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean compare(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
        return (paramAccessibilityNodeInfo.getText() != null) && (nodeText.equals(paramAccessibilityNodeInfo.getText().toString()));
    }
}
