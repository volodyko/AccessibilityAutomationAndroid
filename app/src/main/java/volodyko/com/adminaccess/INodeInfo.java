package volodyko.com.adminaccess;

import android.view.accessibility.AccessibilityNodeInfo;

/**
 * Created by volodyko on 17.03.16.
 */
public interface INodeInfo {
    boolean compare(AccessibilityNodeInfo paramAccessibilityNodeInfo);
}
