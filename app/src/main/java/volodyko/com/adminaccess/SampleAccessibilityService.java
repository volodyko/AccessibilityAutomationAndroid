package volodyko.com.adminaccess;

import android.accessibilityservice.AccessibilityService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.IntDef;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by volodyko on 03.01.17.
 */

public class SampleAccessibilityService extends AccessibilityService {
    private static final String TAG = "SampleAccessibilityServ";
    private UiParser parser;
    private boolean isSecondScreen = false;

    public static final String COMMAND_INTENT = "COMMAND";
    public static final String INTENT_FILTER = "ACCESS";
    public static final int ADMIN_ACCESS = 0;
    public static final int USAGE_STATS = 1;
    public static final int NONE = -1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ADMIN_ACCESS, USAGE_STATS})
    public @interface Command {
    }

    @Command
    private int currentCommand = NONE;


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            currentCommand = intent.getIntExtra(COMMAND_INTENT, NONE);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        parser = new UiParser(getApplicationContext());
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, new IntentFilter(INTENT_FILTER));
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        Log.d(TAG, "onAccessibilityEvent: " + accessibilityEvent);

        AccessibilityNodeInfo source = accessibilityEvent.getSource();

        if (source == null) {
            return;
        }

        switch (currentCommand) {
            case ADMIN_ACCESS:
                adminAccess(source);
                break;
            case USAGE_STATS:
                boolean res = false;
                if (!isSecondScreen) {
                    res = openUsageSettings(source);
                }
                if (res) {
                    isSecondScreen = true;
                }

                if (isSecondScreen) {
                    boolean resClick = false;
                    AccessibilityNodeInfo btnOkNode = parser.findNodeById(source, "android:id/switchWidget");
                    if (btnOkNode != null && btnOkNode.getText().toString().equals("OFF")) {
                        isSecondScreen = false;
                        resClick = btnOkNode.getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    }
                    Log.d(TAG, "onAccessibilityEvent resClick: " + resClick);

                }
                break;
        }
    }

    private void adminAccess(AccessibilityNodeInfo source) {
        AccessibilityNodeInfo btnOkNode = parser.findNodeById(source, "com.android.settings:id/action_button");
        if (btnOkNode != null && btnOkNode.getText().toString().equals("Activate")) {
            Log.d(TAG, "tryFindConfirmDialogNodeInfo: ");
            btnOkNode.performAction(AccessibilityNodeInfo.ACTION_CLICK);
        } else if (btnOkNode != null) {
            btnOkNode.recycle();
        }
    }

    boolean openUsageSettings(AccessibilityNodeInfo nodeInfo) {
        boolean res = false;
        AccessibilityNodeInfo node = parser.findNodeByText(nodeInfo, getApplicationName(getApplicationContext()));
        if (node != null) {
            Log.d(TAG, "openUsageSettings: ");
            res = node.getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
        }
        return res;
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
    }

    public static String getApplicationName(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }
}
